export const routes = (router) => {
    router.post('/posts', async (context) => {
        context.body = {
            owner: 'Jun',
            title: 'The test swagger project',
            body: 'Wow this is a test amazing.'
        };
    });
};