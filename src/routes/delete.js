export const routes = (router) => {
    router.post('/delete', async (context) => {
        context.body = {
            message: 'User deleted.'
        };
    });
};