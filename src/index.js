import koa from 'koa';
import swagger2Koa from 'swagger2-koa';
import * as swagger from 'swagger2';
import Router from '@koa/router';
import koaBody from 'koa-body';

import { routes as registerRoute } from './routes/resgister.js'
import { routes as loginRoute } from './routes/login.js'
import { routes as postRoute } from './routes/posts.js'
import { routes as deleteRoute } from './routes/delete.js'

const { ui, validate } = swagger2Koa;

const spec = swagger.loadDocumentSync('./src/swagger.yaml');

if (!swagger.validateDocument(spec)) {
    throw Error('Swagger not valid')
}

const port = process.env.PORT || 8080;
const app = new koa();

const router = Router({ prefix: '/v1' });

for (const routes of [
    registerRoute,
    loginRoute,
    postRoute,
    deleteRoute
]) {
    routes(router);
}

// optional

router.get('/swagger.json', (context) => {
    context.body = spec;
})


app.use(koaBody());
app.use(validate(spec));
app.use(router.routes());
app.use(router.allowedMethods());
app.use(ui(spec, '/docs'));
app.listen(port, () => {
    console.log(`go to http://localhost:${port}/docs`);
})
